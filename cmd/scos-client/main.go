// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/sensors/scos"
)

func getEnvBool(name string, defValue bool) bool {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseBool(val); err != nil {
			pval = false
		} else {
			return pval
		}
	}
	return defValue
}

func getEnvString(name string, defValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	return defValue
}

type Config struct {
	Debug   bool
	Verbose bool
	BaseUrl string
	Token   string
}

func main() {
	var err error
	config := Config{}

	flag.BoolVar(&config.Debug, "debug",
		getEnvBool("LOG_DEBUG", false), "Enable debug logging.")
	flag.BoolVar(&config.Verbose, "verbose",
		getEnvBool("LOG_VERBOSE", false), "Enable verbose logging.")
	flag.StringVar(&config.BaseUrl, "base-url",
		getEnvString("BASE_URL", "http://localhost"), "Full base url.")
	flag.StringVar(&config.Token, "token",
		getEnvString("TOKEN", ""), "SCOS API token.")
	flag.Parse()

	if config.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else if config.Verbose {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	}

	client := scos.NewScosClient(config.BaseUrl, config.Token, true)

	sar := scos.ScheduleActionRequest{
		Name:   "foo",
		Action: "acquire_m4s_cband_0",
	}
	var resp *scos.ScheduleActionResponse
	if resp, err = client.ScheduleAction(&sar); err != nil {
		log.Fatal().Err(err).Msg("failed to schedule action")
	}
	log.Debug().Msg(fmt.Sprintf("resp = %+v", resp))

	var tresp *scos.TaskCompletedResponse
	for i := 0; i < 80; i += 1 {
		if tresp, err = client.GetTaskCompleted(sar.Name); err != nil {
			time.Sleep(1 * time.Second)
		} else {
			break
		}
	}
	if tresp == nil {
		log.Fatal().Err(err).Msg("failed to get task completed")
	}
	log.Debug().Msg(fmt.Sprintf("resp = %+v", tresp))

	client.DeleteTaskCompleted(sar.Name)
	client.DeleteScheduleAction(sar.Name)

	var dataProducts = tresp.Results[0].Data[0].Metadata.Global.NtiaAlgorithmDataProducts[0]
	seriesLen := dataProducts.Length
	idx := strings.IndexByte(dataProducts.Description, ':')
	sa := strings.Split(dataProducts.Description[idx:], ",")
	sf := make([]float64, 0, seriesLen)
	// Select the proper series: minimum maximum mean median sample
	meanOffset := 2
	for _, sai := range sa[meanOffset*seriesLen : meanOffset*seriesLen+seriesLen] {
		f, _ := strconv.ParseFloat(sai, 64)
		sf = append(sf, f)
	}
	log.Debug().Msg(fmt.Sprintf("%d floats", len(sf)))

	ranges := scos.FindRangesBelowThreshold(sf, -59.0, 10)
	log.Info().Msg(fmt.Sprintf("ranges: %+v", ranges))
}
