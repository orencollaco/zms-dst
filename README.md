<!--
SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
SPDX-License-Identifier: Apache-2.0
-->

# zms-dst (OpenZMS Digital Spectrum Twin service)

The [OpenZMS software](https://gitlab.flux.utah.edu/openzms) is a prototype
automatic spectrum-sharing management system for radio dynamic zones.
OpenZMS provides mechanisms to share electromagnetic (radio-frequency)
spectrum between experimental or test systems and existing spectrum users,
and between multiple experimental systems.  We are building and deploying
OpenZMS within the context of the POWDER wireless testbed in Salt Lake City,
Utah, part of the NSF-sponsored Platforms for Advanced Wireless Research
program, to create [POWDER-RDZ](https://rdz.powderwireless.net).

This `zms-dst` repository contains a /digital spectrum twin/ service that
preserves a complete record of the radio information collected about and
within the zone, from both informational (metrics, maps, observations,
samples, reports, analyses, predictions, etc) and operational (radios,
grants, spectrum, violations, elements, users, events) perspectives.

## Quick dev mode start

Build the project:

```
make
```

The [zms-identity](https://gitlab.flux.utah.edu/openzms/zms-identity) service
verifies tokens and establishes RBAC for OpenZMS.  Therefore, you
must start that service prior to running this service, and must point this
service to the identity service's southbound (gRPC) endpoint at start.

Run the `identity` service as instructed in the
[zms-identity instructions](https://gitlab.flux.utah.edu/openzms/zms-identity).

If you ran the `identity` service with default endpoints, you shouldn't need
to specify any endpoint arguments for the `dst` service.  If you changed the
`identity` service's endpoints, you will need to customize the arguments
below to correspond (see `zms-dst --help`).

Run the `zms-dst` service:

```
mkdir -p tmp
./build/output/zms-dst \
    --db-dsn tmp/zms-dst.sqlite \
    -debug
```
