// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package observations

import (
	"time"
)

type PsdAnnotationValue struct {
	Stat     string    `json:"stat"`
	Length   int       `json:"length"`
	MinFreq  int64     `json:"min_freq"`
	MaxFreq  int64     `json:"max_freq"`
	FreqStep int64     `json:"freq_step"`
	Series   []float64 `json:"series"`
}

type FloatAnnotationValue struct {
    Stat      string     `json:"stat"`
    Value     *float64   `json:"value"`
    Freq      int64      `json:"freq"`
    Bandwidth int64      `json:"bandwidth"`
	StartsAt  *time.Time `json:"starts_at" binding:"omitempty"`
	EndsAt    *time.Time `json:"ends_at" binding:"omitempty,gtfield=StartsAt"`
}


