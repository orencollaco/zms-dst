// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package observations

import (
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
)

type PowderProcessor struct {
	Annotations []*PsdAnnotationValue
}

func NewPowderProcessor() *PowderProcessor {
	return &PowderProcessor{}
}

func Int64Min(x, y int64) int64 {
	if x > y {
		return y
	} else {
		return x
	}
}

func Int64Max(x, y int64) int64 {
	if x > y {
		return x
	} else {
		return y
	}
}

func (pp *PowderProcessor) Check(o *store.Observation) error {
	if len(o.Data) == 0 {
		log.Debug().Msg("no data supplied to powder processor")
		return nil
	}
	data := string(o.Data[:])
	lines := strings.Split(data, "\n")
	if len(lines) < 3 || strings.IndexByte(lines[0], ',') == -1 {
		return fmt.Errorf("expected at least three lines of CSV data")
	}
	var currentAnnotation *PsdAnnotationValue
	currentAnnotation = &PsdAnnotationValue{Stat: "mean"}
	minFreq := int64(math.MaxInt64)
	maxFreq := int64(math.MinInt64)
	firstFreq := int64(-1)
	lastFreq := int64(-1)
	currentCenter := int64(-1)
	lastCenter := int64(-1)
	lastCenterStepSize := int64(-1)
	stepSize := int64(-1)
	minp := math.MaxFloat64
	maxp := math.SmallestNonzeroFloat64
	for i, line := range lines[1:] {
		if line == "" {
			continue
		}
		lv := strings.Split(line, ",")
		if len(lv) < 3 {
			return fmt.Errorf("unexpected CSV value on line %d: need at least two columns (freq, power, center)", i)
		}
		freq, errf := strconv.ParseFloat(lv[0], 64)
		if errf != nil {
			return fmt.Errorf("unexpected CSV value on line %d: first column was not a float (freq): %s", i, errf.Error())
		}
		power, errp := strconv.ParseFloat(lv[1], 64)
		if errp != nil {
			return fmt.Errorf("unexpected CSV value on line %d: second column was not a float (power): %s", i, errp.Error())
		}
		center, errc := strconv.ParseFloat(lv[2], 64)
		if errc != nil {
			return fmt.Errorf("unexpected CSV value on line %d: third column was not a float (center): %s", i, errc.Error())
		}

		// Handle skew in last digit for step size checks; round to nearest sig dig.
		//freqIntStep := int64(math.Round(freq * 100) * 10000)

		freqInt := int64(freq * 1000000)
		if freqInt < maxFreq {
			//log.Debug().Msg(fmt.Sprintf("skipping overlapping freq %d (%d)", freqInt, maxFreq))
			continue
		}
		minFreq = Int64Min(minFreq, freqInt)
		maxFreq = Int64Max(maxFreq, freqInt)
		currentCenter = int64(center * 1000000)
		if lastCenter == -1 {
			lastCenter = currentCenter
		}
		if lastCenter != currentCenter {
			// add this center sample as a separate annotation
			currentAnnotation.Length = len(currentAnnotation.Series)
			currentAnnotation.MinFreq = firstFreq
			currentAnnotation.MaxFreq = lastFreq
			currentAnnotation.FreqStep = stepSize
			pp.Annotations = append(pp.Annotations, currentAnnotation)

			lastCenter = currentCenter

			// start a new annotation
			currentAnnotation = &PsdAnnotationValue{Stat: "mean"}
			lastCenterStepSize = stepSize
			stepSize = -1
			firstFreq = -1
			lastFreq = -1
		}
		if firstFreq == -1 {
			firstFreq = freqInt
		}
		if lastFreq == -1 {
			lastFreq = freqInt
		} else {
			thisStep := freqInt - lastFreq
			if stepSize == -1 {
				stepSize = thisStep
				if false && lastCenterStepSize != -1 && lastCenterStepSize != stepSize {
					return fmt.Errorf("unexpected CSV value on line %d: step size from prior center (%d) not consistent with new step size (%d)", i, lastCenterStepSize, stepSize)
				}
			} else {
				if false && stepSize != thisStep {
					return fmt.Errorf("unexpected CSV value on line %d: step size (%d) not consistent with initial step size (%d)", i, thisStep, stepSize)
				}
			}
			lastFreq = freqInt
		}

		minp = math.Min(minp, power)
		maxp = math.Max(maxp, power)

		currentAnnotation.Series = append(currentAnnotation.Series, power)
	}
	// Add a pending, final annotation
	if len(currentAnnotation.Series) > 0 {
		currentAnnotation.Length = len(currentAnnotation.Series)
		currentAnnotation.MinFreq = firstFreq
		currentAnnotation.MaxFreq = lastFreq
		currentAnnotation.FreqStep = stepSize
		pp.Annotations = append(pp.Annotations, currentAnnotation)
	}
	if o.MinFreq != minFreq {
		return fmt.Errorf("observation.MinFreq (%d) does not match data minFreq (%d)", o.MinFreq, minFreq)
	}
	if o.MaxFreq != maxFreq {
		return fmt.Errorf("observation.MaxFreq (%d) does not match data maxFreq (%d)", o.MaxFreq, maxFreq)
	}

	log.Debug().Msg(fmt.Sprintf("observation psd annotations: count=%d, min=%f, max=%f)", len(pp.Annotations), minp, maxp))

	return nil
}

func (pp *PowderProcessor) Update(o *store.Observation) (bool, error) {
	if o.Violation {
		t := time.Now().UTC()
		o.ViolationVerifiedAt = &t
		return true, nil
	}
	return false, nil
}

func (pp *PowderProcessor) Annotate(o *store.Observation) (annotations []*store.Annotation, err error) {
	annotations = make([]*store.Annotation, 0, len(pp.Annotations))
	for _, a := range pp.Annotations {
		var encValue []byte
		if encValue, err = json.Marshal(a); err != nil {
			return nil, fmt.Errorf("failed to convert powder csv observation into psd Annotation: %+v", err)
		}
		ann := store.Annotation{
			Type:  "psd-mean",
			Name:  "powder-csv-mean",
			Value: encValue,
		}
		annotations = append(annotations, &ann)
	}

	return annotations, nil
}
