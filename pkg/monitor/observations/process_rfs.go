// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package observations

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
)

type RfsProcessor struct {
	Annotations []*FloatAnnotationValue
}

func NewRfsProcessor() *RfsProcessor {
	return &RfsProcessor{}
}



func (pp *RfsProcessor) Check(o *store.Observation) error {
	if len(o.Data) == 0 {
		log.Debug().Msg("no data supplied to rfs processor")
		return nil
	}
	data := string(o.Data[:])
	lines := strings.Split(data, "\n")

	/* Check if the CSV data has at least two lines (header and a data line) and a comma */
	if len(lines) < 2 || strings.IndexByte(lines[0], ',') == -1 {
		return fmt.Errorf("expected at least header and one data line of CSV data")
	}

	/* Create a new power and kurtosis annotation pointers */
	var currentKurtosisAnnotation *FloatAnnotationValue
	var currentPowerAnnotation *FloatAnnotationValue
	
	/* Iterate through each line */
	for i, line := range lines[1:] {
		if line == "" {
			continue
		}
		lv := strings.Split(line, ",")
		if len(lv) < 3 {
			return fmt.Errorf("unexpected CSV value on line %d: need at least two columns (freq, power, kurtosis)", i)
		}
		freq, errf := strconv.ParseFloat(lv[0], 64)
		if errf != nil {
			return fmt.Errorf("unexpected CSV value on line %d: zeroth column was not a float (freq): %s", i, errf.Error())
		}
		power, errp := strconv.ParseFloat(lv[1], 64)
		if errp != nil {
			return fmt.Errorf("unexpected CSV value on line %d: first column was not a float (power): %s", i, errp.Error())
		}
		kurtosis, errk := strconv.ParseFloat(lv[6], 64)
		if errk != nil {
			return fmt.Errorf("unexpected CSV value on line %d: sixth column was not a float (kurtosis): %s", i, errk.Error())
		}
		bandwidth, errb := strconv.ParseFloat(lv[7], 64)
		if errb != nil {
			return fmt.Errorf("unexpected CSV value on line %d: seventh column was not a float (bandwidth): %s", i, errb.Error())
		}
		/* Parse created datetime in format 2024-03-06 15:47:38+00:00 */
		created, errd := time.Parse("2006-01-02 15:04:05-07:00", lv[11])
		if errd != nil {
			return fmt.Errorf("unexpected CSV value on line %d: eleventh column was not a time in 2006-01-02 15:04:05-07:00 format (time): %s", i, errd.Error())
		}

		/* Create a new power and kurtosis annotation */
		currentPowerAnnotation = &FloatAnnotationValue{Stat: "rfs.avg_power"}
		currentKurtosisAnnotation = &FloatAnnotationValue{Stat: "rfs.kurtosis"}

		/* Add the power, freq, bandwidth and datetime to the current power annotation */
		currentPowerAnnotation.Value = &power
		currentPowerAnnotation.Freq = int64(freq)
		currentPowerAnnotation.Bandwidth = int64(bandwidth)
		currentPowerAnnotation.StartsAt = &created
		currentPowerAnnotation.EndsAt = &created

		/* Add the kurtosis, freq, bandwidth and datetime to the current kurtosis annotation */
		currentKurtosisAnnotation.Value = &kurtosis
		currentKurtosisAnnotation.Freq = int64(freq)
		currentKurtosisAnnotation.Bandwidth = int64(bandwidth)
		currentKurtosisAnnotation.StartsAt = &created
		currentKurtosisAnnotation.EndsAt = &created

		/* Print currentPowerAnnotation and currentKurtosisAnnotation for debugging */
		// log.Debug().Msg(fmt.Sprintf("observation rfs flt annotations: power=%f, freq=%d, bandwidth=%d, created=%s)", *currentPowerAnnotation.Value, currentPowerAnnotation.Freq, currentPowerAnnotation.Bandwidth, currentPowerAnnotation.StartsAt.String()))
		// log.Debug().Msg(fmt.Sprintf("observation rfs flt annotations: kurtosis=%f, freq=%d, bandwidth=%d, created=%s)", *currentKurtosisAnnotation.Value, currentKurtosisAnnotation.Freq, currentKurtosisAnnotation.Bandwidth, currentKurtosisAnnotation.StartsAt.String()))

		/* Append the power and kurtosis annotations to the list of annotations */
		pp.Annotations = append(pp.Annotations, currentPowerAnnotation)
		pp.Annotations = append(pp.Annotations, currentKurtosisAnnotation)
	
	}
	log.Debug().Msg(fmt.Sprintf("observation rfs flt annotations: count=%d)", len(pp.Annotations)))

	return nil
}

func (pp *RfsProcessor) Update(o *store.Observation) (bool, error) {
	if o.Violation {
		t := time.Now().UTC()
		o.ViolationVerifiedAt = &t
		return true, nil
	}
	return false, nil
}

func (pp *RfsProcessor) Annotate(o *store.Observation) (annotations []*store.Annotation, err error) {
	annotations = make([]*store.Annotation, 0, len(pp.Annotations))
	for _, a := range pp.Annotations {
		var encValue []byte
		if encValue, err = json.Marshal(a); err != nil {
			return nil, fmt.Errorf("failed to convert rfs csv observation into flt Annotation: %+v", err)
		}
		ann := store.Annotation{
			Type:  "rfs-mean",
			Name:  "rfs-csv-mean",
			Value: encValue,
		}
		annotations = append(annotations, &ann)
	}

	return annotations, nil
}
