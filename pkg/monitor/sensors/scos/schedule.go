// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package scos

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

type ScheduleActionRequest struct {
	Name         string     `json:"name"`
	Action       string     `json:"action"`
	Priority     *int       `json:"priority,omitempty"`
	Start        *time.Time `json:"start,omitempty"`
	Stop         *time.Time `json:"stop,omitempty"`
	RelativeStop *int       `json:"relative_stop,omitempty"`
	Interval     *int       `json:"interval,omitempty"`
	CallbackUrl  *string    `json:"callback_url,omitempty"`
	ValidateOnly *bool      `json:"validate_only,omitempty"`
}

type ScheduleActionResponse struct {
	Schedule Schedule `json:"schedule"`
}

func (client *ScosClient) ScheduleAction(req *ScheduleActionRequest) (resp *ScheduleActionResponse, err error) {
	var body []byte
	if body, err = json.Marshal(req); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("ScheduleAction http req body = %+v", body))
	hreq, err := http.NewRequest(
		"POST", client.BaseApiUrl+"/schedule/", bytes.NewReader(body))
	hreq.Header.Set("Authorization", "Token "+client.Token)
	hreq.Header.Set("Content-Type", "application/json")
	log.Debug().Msg(fmt.Sprintf("ScheduleAction http req = %+v", hreq))
	var hresp *http.Response
	if hresp, err = client.Client.Do(hreq); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("ScheduleAction http resp = %+v", hresp))
	if hresp.StatusCode != 201 {
		return nil, NewScosClientError(errors.New(hresp.Status), hresp.StatusCode)
	}
	if body, err = ioutil.ReadAll(hresp.Body); err != nil {
		return nil, err
	}

	resp = new(ScheduleActionResponse)
	if err = json.Unmarshal(body, &resp.Schedule); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("ScheduleAction resp = %+v", resp))
	return resp, nil
}

func (client *ScosClient) ScheduleActionBatch(req []*ScheduleActionRequest) (resp []*ScheduleActionResponse, err error) {
	return nil, nil
}

func (client *ScosClient) DeleteScheduleAction(name string) (err error) {
	log.Debug().Msg(fmt.Sprintf("DeleteScheduleAction name %s", name))
	hreq, err := http.NewRequest(
		"DELETE", client.BaseApiUrl+"/schedule/"+name+"/", nil)
	hreq.Header.Set("Authorization", "Token "+client.Token)
	hreq.Header.Set("Content-Type", "application/json")
	log.Debug().Msg(fmt.Sprintf("DeleteScheduleAction http req = %+v", hreq))
	var hresp *http.Response
	if hresp, err = client.Client.Do(hreq); err != nil {
		return err
	}
	log.Debug().Msg(fmt.Sprintf("DeleteScheduleAction http resp = %+v", hresp))
	if hresp.StatusCode != 204 {
		return NewScosClientError(errors.New(hresp.Status), hresp.StatusCode)
	}
	return nil
}
