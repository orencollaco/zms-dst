// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"google.golang.org/protobuf/types/known/timestamppb"

	dst_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
)

// NB: caller must Preload any relations; elaborate parameter only controls
// output of them, to give caller more flexibility.

func (in *Annotation) ToProto(out *dst_v1.Annotation, elaborate bool) error {
	out.Id = in.Id.String()
	out.ObservationId = in.ObservationId.String()
	out.Type = in.Type
	out.Name = in.Name
	out.Description = in.Description
	out.Value = in.Value.String()
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}

	return nil
}
func (in *Observation) ToProto(out *dst_v1.Observation, elaborate bool) error {
	out.Id = in.Id.String()
	out.MonitorId = in.MonitorId.String()
	if in.CollectionId != nil {
		s := in.CollectionId.String()
		out.CollectionId = &s
	}
	out.Types = in.Types
	out.Format = in.Format
	out.Description = in.Description
	out.MinFreq = in.MinFreq
	out.MaxFreq = in.MaxFreq
	out.FreqStep = in.FreqStep
	out.StartsAt = timestamppb.New(in.StartsAt)
	if in.EndsAt != nil {
		out.EndsAt = timestamppb.New(*in.EndsAt)
	}
	out.Data = in.Data
	out.Violation = in.Violation
	if in.ViolationVerifiedAt != nil {
		out.ViolationVerifiedAt = timestamppb.New(*in.ViolationVerifiedAt)
	}
	out.Interference = in.Interference
	if in.InterferenceVerifiedAt != nil {
		out.InterferenceVerifiedAt = timestamppb.New(*in.InterferenceVerifiedAt)
	}
	out.ElementId = in.ElementId.String()
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate && len(in.Annotations) > 0 {
		for _, in_ann := range in.Annotations {
			out_ann := new(dst_v1.Annotation)
			in_ann.ToProto(out_ann, elaborate)
			out.Annotations = append(out.Annotations, out_ann)
		}
	}
	if in.ViolatedGrantId != nil {
		s := in.ViolatedGrantId.String()
		out.ViolatedGrantId = &s
	}

	return nil
}
