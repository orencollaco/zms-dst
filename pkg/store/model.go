// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type AreaPoint struct {
	Id     uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	AreaId uuid.UUID `gorm:"foreignKey:AreaId;type:uuid" json:"area_id"`
	X      float64   `json:"x" binding:"required"`
	Y      float64   `json:"y" binding:"required"`
	Z      float64   `json:"z"`
}

func (x *AreaPoint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Area struct {
	Id          uuid.UUID   `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId   uuid.UUID   `gorm:"type:uuid" json:"element_id" binding:"required"`
	Name        string      `gorm:"size:256" json:"name" binding:"required"`
	Description string      `gorm:"size:256" json:"description"`
	Srid        int         `json:"srid" binding:"required"`
	Points      []AreaPoint `json:"points"`
}

func (x *Area) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Annotation struct {
	Id            uuid.UUID      `gorm:"primaryKey;type:uuid" json:"id"`
	ObservationId uuid.UUID      `gorm:"foreignKey:ObservationId;type:uuid" json:"observation_id" binding:"required"`
	Type          string         `gorm:"size:512" json:"type" binding:"required"`
	Name          string         `gorm:"size:512" json:"name" binding:"required"`
	Description   string         `gorm:"size:4096" json:"description"`
	Value         datatypes.JSON `json:"value"`
	CreatorId     uuid.UUID      `gorm:"type:uuid" json:"creator_id"`
	UpdaterId     *uuid.UUID     `gorm:"type:uuid" json:"updater_id"`
	CreatedAt     time.Time      `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt     *time.Time     `gorm:"autoUpdateTime" json:"updated_at"`
}

func (x *Annotation) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Observation struct {
	Id                     uuid.UUID    `gorm:"primaryKey;type:uuid" json:"id"`
	MonitorId              uuid.UUID    `gorm:"type:uuid" json:"monitor_id" binding:"required"`
	CollectionId           *uuid.UUID   `gorm:"foreignKey:CollectionId;type:uuid" json:"collection_id"`
	Types                  string       `gorm:"size:512" json:"types" binding:"required"`
	Format                 string       `gorm:"size:512" json:"format" binding:"required"`
	Description            string       `gorm:"size:4096" json:"description"`
	MinFreq                int64        `json:"min_freq"`
	MaxFreq                int64        `json:"max_freq"`
	FreqStep               int64        `json:"freq_step"`
	StartsAt               time.Time    `json:"starts_at" binding:"required"`
	EndsAt                 *time.Time   `json:"ends_at" binding:"omitempty,gtfield=StartsAt"`
	Data                   []byte       `json:"data" binding:"required"`
	Violation              bool         `json:"violation"`
	ViolationVerifiedAt    *time.Time   `json:"violation_verified_at"`
	Interference           bool         `json:"interference"`
	InterferenceVerifiedAt *time.Time   `json:"interference_verified_at"`
	ElementId              uuid.UUID    `gorm:"type:uuid" json:"element_id"`
	CreatorId              uuid.UUID    `gorm:"type:uuid" json:"creator_id"`
	UpdaterId              *uuid.UUID   `gorm:"type:uuid" json:"updater_id"`
	CreatedAt              time.Time    `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt              *time.Time   `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt              *time.Time   `gorm:"autoUpdateTime" json:"deleted_at"`
	Annotations            []Annotation `json:"annotations"`
	ViolatedGrantId        *uuid.UUID   `gorm:"type:uuid" json:"violated_grant_id" binding:"-"`
	Path                   string       `gorm:"size:512" json:"-" binding:"-"`
}

func (x *Observation) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Collection struct {
	Id              uuid.UUID     `gorm:"primaryKey;type:uuid" json:"id"`
	MonitorId       uuid.UUID     `gorm:"type:uuid" json:"monitor_id" binding:"required"`
	Types           string        `gorm:"size:512" json:"types" binding:"required"`
	Format          string        `gorm:"size:512" json:"format" binding:"required"`
	Description     string        `gorm:"size:4096" json:"description"`
	MinFreq         int64         `json:"min_freq"`
	MaxFreq         int64         `json:"max_freq"`
	StartsAt        time.Time     `json:"starts_at" binding:"required"`
	EndsAt          *time.Time    `json:"ends_at" binding:"omitempty,gtfield=StartsAt"`
	Violation       bool          `json:"violation"`
	Interference    bool          `json:"interference"`
	ElementId       uuid.UUID     `gorm:"type:uuid" json:"element_id"`
	CreatorId       uuid.UUID     `gorm:"type:uuid" json:"creator_id"`
	UpdaterId       *uuid.UUID    `gorm:"type:uuid" json:"updater_id"`
	CreatedAt       time.Time     `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt       *time.Time    `gorm:"autoUpdateTime" json:"updated_at"`
	Observations    []Observation `json:"observations"`
	ViolatedGrantId *uuid.UUID    `gorm:"type:uuid" json:"violated_grant_id" binding:"-"`
}

func (x *Collection) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type PropsimJobOutputType string

const (
	Raw     PropsimJobOutputType = "raw"
	GeoTIFF PropsimJobOutputType = "geotiff"
)

func CreatePropsimJobOutputTypeEnum(db *gorm.DB) (err error) {
	db.Debug().Exec(`
	DO $$ BEGIN
		CREATE TYPE propsim_job_output_type_enum AS ENUM ('raw', 'geotiff');
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`)
	return nil
}

type PropsimRaster struct {
	Rid  int    `gorm:"type:serial;primaryKey;autoIncrement" json:"-"`
	Rast []byte `gorm:"type:raster" json:"-"`
}

type PropsimJobOutput struct {
	Id               uuid.UUID            `gorm:"primaryKey;type:uuid" json:"id"`
	PropsimJobId     uuid.UUID            `gorm:"foreignKey:PropsimJobId;type:uuid" json:"prop_sim_job_id"`
	Type             PropsimJobOutputType `sql:"type:propsim_job_output_type_enum"`
	Name             string               `json:"name"`
	Desc             string               `json:"desc"`
	Size             uint64               `json:"size"`
	Path             string               `json:"-"`
	PropsimRasterRid *int                 `gorm:"foreignKey:PropsimRasterRid;references:Rid" json:"-"`
	AreaId           *uuid.UUID           `gorm:"foreignKey:AreaId;type:uuid" json:"area_id"`
	Area             *Area                `json:"area"`
}

type PropsimJobStatus string

const (
	Created   PropsimJobStatus = "created"
	Scheduled PropsimJobStatus = "scheduled"
	Running   PropsimJobStatus = "running"
	Complete  PropsimJobStatus = "completed"
	Failed    PropsimJobStatus = "failed"
	Deleted   PropsimJobStatus = "deleted"
)

func CreatePropsimJobStatusEnum(db *gorm.DB) (err error) {
	db.Debug().Exec(`
	DO $$ BEGIN
		CREATE TYPE propsim_job_status_enum AS ENUM ('created', 'scheduled', 'running', 'completed', 'failed', 'deleted');
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`)
	return nil
}

// XXX: need to "connect" the radio/antenna/location info for each tx/rx from the zmc and high-level api
// given a tx radio id, power, what are common params?  what are sim-specific?  how to handle data type conv?  maybe marshal into propsim messages immediately; if fail, return sync error to API caller

type PropsimJob struct {
	Id                   uuid.UUID          `gorm:"primaryKey;type:uuid" json:"id"`
	ServiceId            *uuid.UUID         `gorm:"type:uuid" json:"service_id"`
	JobId                *uuid.UUID         `gorm:"type:uuid" json:"job_id"`
	RadioPortId          uuid.UUID          `gorm:"type:uuid" json:"radio_port_id"`
	Freq                 int64              `json:"freq"`
	Gain                 float32            `json:"gain"`
	CreatorId            uuid.UUID          `gorm:"type:uuid" json:"creator_id"`
	ElementId            *uuid.UUID         `gorm:"type:uuid" json:"element_id"`
	Status               PropsimJobStatus   `sql:"type:propsim_job_status_enum"`
	CreatedAt            time.Time          `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt            *time.Time         `json:"updated_at"`
	FinishedAt           *time.Time         `json:"finished_at"`
	DeletedAt            *time.Time         `json:"deleted_at"`
	CompletionPercentage float64            `json:"completion_percentage"`
	Outputs              []PropsimJobOutput `json:"outputs"`
}

func (x *PropsimJob) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}
