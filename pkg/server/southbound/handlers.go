// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package southbound

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	dst "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/observations"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/sensors/scos"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
)

func (s *Server) GetObservation(ctx context.Context, req *dst.GetObservationRequest) (resp *dst.GetObservationResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid ObservationRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid observation id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetObservation(%+v)", req))

	var item store.Observation
	q := s.db.Where(&store.Observation{Id: id})
	if elaborate {
		q = q.Preload("Annotations")
	}
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Observation not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	var outval dst.Observation
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Observation")
	}

	resp = &dst.GetObservationResponse{
		Header:      &dst.ResponseHeader{},
		Observation: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetObservations(ctx context.Context, req *dst.GetObservationsRequest) (resp *dst.GetObservationsResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid ObservationRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	log.Info().Msg(fmt.Sprintf("GetObservations(%+v)", req))

	return nil, status.Errorf(codes.Unimplemented, "unimplemented")
}

func (s *Server) CollectSpectrumObservation(ctx context.Context, minFreq int64, maxFreq int64) (collection *store.Collection, err error) {
	var zmcClient zmc.ZmcClient
	if zmcClient, err = s.rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	var monitor zmc.Monitor
	stype := "scos"
	if resp, err := zmcClient.GetMonitors(ctx, &zmc.GetMonitorsRequest{Types: &stype}); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain monitors from zmc service")
	} else if resp.Monitors == nil || len(resp.Monitors) == 0 {
		return nil, status.Errorf(codes.Internal, "No monitors available from zmc service")
	} else {
		monitor = *resp.Monitors[0]
	}

	var scosClient *scos.ScosClient
	if scosClient, err = scos.ScosClientFromMonitor(&monitor); err != nil {
		return nil, status.Errorf(codes.Internal, "cannot generate scos client from monitor: %+v", err)
	}
	//log.Debug().Msg(fmt.Sprintf("scos client: %+v", scosClient))

	if collection, err = scosClient.CollectSpectrumFft(minFreq, maxFreq); err != nil {
		return nil, status.Errorf(codes.Internal, "cannot collect spectrum occupancy data: %+v", err)
	}
	if res := s.db.Create(collection); res.Error != nil {
		return nil, status.Errorf(codes.Internal, "error storing scos observations: %+v", res.Error)
	}
	log.Debug().Msg(fmt.Sprintf("collection: %+v (%d, %d, %+v, %+v", collection.Id, collection.MinFreq, collection.MaxFreq, collection.StartsAt, collection.EndsAt))

	return collection, nil
}

func Int64Max(x, y int64) int64 {
	if x > y {
		return x
	} else {
		return y
	}
}

func Int64Min(x, y int64) int64 {
	if x < y {
		return x
	} else {
		return y
	}
}

// Input ranges must be presorted.
func IntersectRangeLists(old [][2]int64, new [][2]int64) (ret [][2]int64) {
	ret = make([][2]int64, 0, 0)

	// If either input list is empty, the intersection is nil.
	if old == nil || len(old) == 0 {
		return ret
	} else if new == nil || len(new) == 0 {
		return ret
	}

	oi := 0
	for _, nv := range new {
		nx := nv[0]
		ny := nv[1]
		if oi == len(old) {
			log.Debug().Msg(fmt.Sprintf("new [%d,%d] above all old; skipping remaining new", nx, ny))
			break
		}
		for oi < len(old) {
			ox := old[oi][0]
			oy := old[oi][1]
			if ny <= ox {
				// New range is below old range; skip entirely.
				log.Debug().Msg(fmt.Sprintf("new [%d,%d] below old [%d,%d]; skipping new", nx, ny, ox, oy))
				break
			} else if nx >= oy {
				// New range is above current old range; skip old range.
				log.Debug().Msg(fmt.Sprintf("new [%d,%d] above old [%d,%d]; skipping old", nx, ny, ox, oy))
				oi += 1
				continue
			} else {
				// Current new and and old overlap; compute intersection and
				// add that.
				mx := Int64Max(nx, ox)
				my := Int64Min(ny, oy)
				log.Debug().Msg(fmt.Sprintf("new [%d,%d] overlaps old [%d,%d]; merged [%d,%d]", nx, ny, ox, oy, mx, my))
				ret = append(ret, [2]int64{mx, my})
				// Continue -- there could be a subsequent old range that
				// also overlaps with this new range.
				oi += 1
				continue
			}
		}
	}

	return ret
}

func (s *Server) GetUnoccupiedSpectrum(ctx context.Context, req *dst.GetUnoccupiedSpectrumRequest) (resp *dst.GetUnoccupiedSpectrumResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetUnoccupiedSpectrumRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var monitorIdList []*uuid.UUID
	if req.MonitorIds != nil {
		for _, mId := range req.MonitorIds {
			if idParsed, err := uuid.Parse(mId); err != nil {
				return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid GetUnoccupiedSpectrumRequest: invalid monitor_ids item '%s'", mId))
			} else {
				monitorIdList = append(monitorIdList, &idParsed)
			}
		}
	}

	log.Info().Msg(fmt.Sprintf("GetUnoccupiedSpectrum(%+v)", req))

	//
	// If there are recent observations, simply use those.  Otherwise, generate some.
	//
	var collections []store.Collection
	q := s.db.Where(
		s.db.Where(store.MakeILikeClause(s.config, "Collections.types"), "%scos%").
			Or(store.MakeILikeClause(s.config, "Collections.format"), "%psd-csv-ota%")).
		Where("Collections.min_freq <= ?", req.MinFreq).
		Where("Collections.max_freq >= ?", req.MaxFreq)
	if req.Type != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Collections.types"), fmt.Sprintf("%%%s%%", *req.Type))
	}
	if req.Format != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Collections.formats"), fmt.Sprintf("%%%s%%", *req.Format))
	}
	if req.Type == nil && req.Format == nil {
		q = q.Where(s.db.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Collections.types"), "%scos%").
				Or(store.MakeILikeClause(s.config, "Collections.format"), "%psd-csv-ota%")))
	}
	if monitorIdList != nil && len(monitorIdList) > 0 {
		q = q.Where("Collections.monitor_id in ?", monitorIdList)
	}
	q = q.Preload("Observations", func(db *gorm.DB) *gorm.DB {
			return db.Order("observations.min_freq asc")
		}).
		Preload("Observations.Annotations", func(db *gorm.DB) *gorm.DB {
			return db.Where("Annotations.type = 'psd-mean'")
		}).
		Order("Collections.starts_at desc")
	if req.StartsAt != nil {
		q = q.Where("Collections.starts_at > ?", req.StartsAt.AsTime())
		if req.EndsAt != nil {
			q = q.Where("Collections.starts_at < ?", req.EndsAt.AsTime())
		}
	} else {
		q = q.Limit(1)
	}
	if qres := q.Find(&collections); qres.RowsAffected < 1 {
		if collection, qerr := s.CollectSpectrumObservation(ctx, req.MinFreq, req.MaxFreq); qerr != nil {
			return nil, status.Errorf(codes.Internal, "error collecting spectrum: %+v", qerr)
		} else {
			collections = append(collections, *collection)
		}
	}

	// Walk the collections and look for sub-threshold occupancy ranges
	var merged [][2]int64
	minStartsAt := time.Now().UTC()
	for _, collection := range collections {
		ranges := make([][2]int64, 0)
		var minf int64 = 0
		var maxf int64 = 0
		log.Debug().Msg(fmt.Sprintf("checking collection %+v", collection.Id))
		for _, o := range collection.Observations {
			log.Debug().Msg(fmt.Sprintf("checking observation %v-%v", o.MinFreq, o.MaxFreq))
			//if minf == 0 {
			//	minf = o.MinFreq
			//}
			var annValues []observations.PsdAnnotationValue
			for _, a := range o.Annotations {
				var annValue observations.PsdAnnotationValue
				if err = json.Unmarshal(a.Value, &annValue); err != nil {
					return nil, status.Errorf(codes.Internal, "bad psd-mean annotation value: %+v", err)
				}
				annValues = append(annValues, annValue)
			}
			sort.SliceStable(annValues, func(i, j int) bool {
				if annValues[i].MinFreq < annValues[j].MinFreq {
					return true
				} else {
					return false
				}
			})

			for _, annValue := range annValues {
				step := (annValue.MaxFreq - annValue.MinFreq) / int64(len(annValue.Series))

				//log.Debug().Msg(fmt.Sprintf("data: %+v", annValue.Series))

				for i := 0; i < len(annValue.Series); i += 1 {
					//log.Debug().Msg(fmt.Sprintf("CSV: %v,%v", annValue.MinFreq+int64(i)*step, annValue.Series[i]))
					if annValue.Series[i] > req.OccupancyThreshold {
						//log.Debug().Msg(fmt.Sprintf("above: %v (%v)", annValue.MinFreq+int64(i)*step, annValue.Series[i]))
						if minf != 0 {
							if req.MinBandwidth == nil || (maxf-minf) > *req.MinBandwidth {
								log.Debug().Msg(fmt.Sprintf("adding range: %v-%v", minf, maxf)) //, annValue.Series[i]))
								ranges = append(ranges, [2]int64{minf, maxf})
							} else {
								//log.Debug().Msg(fmt.Sprintf("skipping range: %v-%v", minf, maxf)) //, annValue.Series[i]))
							}
							minf = 0
							maxf = 0
						}
					} else {
						if minf == 0 {
							minf = annValue.MinFreq + int64(i)*step
							log.Debug().Msg(fmt.Sprintf("starting range: %v (%v)", minf, annValue.Series[i]))
						}
						maxf = annValue.MinFreq + int64(i)*step
					}
				}
			}
		}
		if minf != 0 && (req.MinBandwidth == nil || (maxf-minf) > *req.MinBandwidth) {
			ranges = append(ranges, [2]int64{minf, maxf})
			minf = 0
			maxf = 0
		}

		log.Debug().Msg(fmt.Sprintf("collection ranges: %+v", ranges))

		if collection.StartsAt.Before(minStartsAt) {
			minStartsAt = collection.StartsAt
		}

		if merged == nil || len(merged) == 0 {
			merged = ranges
		} else {
			merged = IntersectRangeLists(merged, ranges)
			log.Debug().Msg(fmt.Sprintf("merged: %+v", merged))

			if len(merged) == 0 {
				log.Debug().Msg(fmt.Sprintf("merged: empty, aborting"))
				break
			}
		}
	}

	// Ensure that our bounds all respect MinFreq/MaxFreq/Bandwidth; the
	// query above may have processed data outside of the ranges of the
	// original request.
	if merged != nil && len(merged) > 0 {
		filtered := make([][2]int64, 0, len(merged))
		for _, m := range merged {
			if m[0] < req.MinFreq {
				m[0] = req.MinFreq
			}
			if m[1] > req.MaxFreq {
				m[1] = req.MaxFreq
			}
			d := m[1] - m[0]
			if d > 0 || (req.MinBandwidth != nil && d < *req.MinBandwidth) {
				filtered = append(filtered, [2]int64{m[0], m[1]})
			}
		}
		log.Debug().Msg(fmt.Sprintf("filtered: %+v", filtered))
		merged = filtered
	}

	resp = &dst.GetUnoccupiedSpectrumResponse{
		Header:   &dst.ResponseHeader{},
		StartsAt: timestamppb.New(minStartsAt),
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	for _, r := range merged {
		resp.Ranges = append(resp.Ranges, &dst.FrequencyRange{MinFreq: r[0], MaxFreq: r[1]})
	}

	return resp, nil
}

func (s *Server) GetTxConstraints(ctx context.Context, req *dst.GetTxConstraintsRequest) (resp *dst.GetTxConstraintsResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetTxConstraintsRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var radioPortId uuid.UUID
	if radioPortId, err = uuid.Parse(req.RadioPortId); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid radio_port_id")
	}

	log.Info().Msg(fmt.Sprintf("GetTxConstraints(%+v)", req))

	// Allow some slop in the simulation vs target frequency
	minFreq := req.Freq - 100*1000000
	maxFreq := req.Freq + 100*1000000

	var propsimJobOutput store.PropsimJobOutput
	var propsimJob store.PropsimJob
	//q := s.db.Where("propsim_jobs.radio_port_id=?", radioPortId).
	q := s.db.
		Joins("left join propsim_jobs on propsim_jobs.id=propsim_job_outputs.propsim_job_id").
		Where("propsim_jobs.radio_port_id=?", radioPortId).
		Where("propsim_jobs.deleted_at is NULL").
		Where("propsim_jobs.status = ?", store.Complete).
		Where("propsim_jobs.freq >= ?", minFreq).
		Where("propsim_jobs.freq <= ?", maxFreq).
		Where("propsim_job_outputs.propsim_raster_rid is not NULL").
		Order("propsim_jobs.finished_at desc")
	if qres := q.First(&propsimJobOutput, &propsimJob); qres.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "no matching simulation: %+v", err)
	}
	type Result struct {
		Max float64
	}
	q = s.db.Raw("select (ST_SummaryStats(ST_Clip(psr.rast,ST_Difference(ST_ConvexHull(psr.rast), ST_ConvexHull(ST_Polygon('LINESTRING(-111.82639 40.77309, -111.85250 40.77313, -111.85250 40.75096, -111.82661 40.75076, -111.82639 40.77309)'::geometry, 4326)))::geometry))).max as max from propsim_rasters as psr where psr.rid = ?", propsimJobOutput.PropsimRasterRid)
	var res Result
	if qres := q.First(&res); qres.RowsAffected != 1 {
		return nil, status.Errorf(codes.Internal, "no matching raster for simulation %d", propsimJobOutput.PropsimRasterRid)
	}

	resp = &dst.GetTxConstraintsResponse{
		Header:   &dst.ResponseHeader{},
		MaxPower: res.Max,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	log.Info().Msg(fmt.Sprintf("GetTxConstraints(%+v): resp %+v", req.Header.ReqId, resp))

	return resp, nil
}

func EventToProto(e *subscription.Event, include bool, elaborate bool) (*dst.Event, error) {
	ep := &dst.Event{
		Header: e.Header.ToProto(),
	}

	if include && e.Object != nil {
		switch v := e.Object.(type) {
		case *store.Observation:
			x := new(dst.Observation)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &dst.Event_Observation{Observation: x}
		case *store.Annotation:
			x := new(dst.Annotation)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &dst.Event_Annotation{Annotation: x}
		default:
			return nil, fmt.Errorf("unknown event object type %T", v)
		}
	}

	log.Debug().Msg(fmt.Sprintf("EventToProto: %+v", ep))

	return ep, nil
}

func (s *Server) Subscribe(req *dst.SubscribeRequest, stream dst.Dst_SubscribeServer) error {
	if req == nil || req.Header == nil || req.Header.ReqId == nil {
		return status.Errorf(codes.InvalidArgument, "Invalid SubscribeRequest")
	}
	if err := req.Validate(); err != nil {
		return status.Errorf(codes.InvalidArgument, err.Error())
	}

	var filters []subscription.EventFilter
	if req.Filters != nil && len(req.Filters) > 0 {
		filters = make([]subscription.EventFilter, 0, len(req.Filters))
		for _, f := range req.Filters {
			filters = append(filters, subscription.EventFilterFromProto(f))
		}
	}
	ch := make(chan *subscription.Event)
	sid := *req.Header.ReqId
	endpoint := ""
	if p, ok := peer.FromContext(stream.Context()); ok {
		endpoint = p.Addr.String()
	}
	if err := s.sm.Subscribe(sid, filters, ch, endpoint, subscription.Rpc, nil); err != nil {
		return status.Errorf(codes.Internal, err.Error())
	}

	// Handle write panics as well as the regular return cases.
	defer func() {
		if true || recover() != nil {
			s.sm.Unsubscribe(sid)
		}
	}()

	include := req.Include == nil || *req.Include
	elaborate := req.Header.Elaborate == nil || *req.Header.Elaborate

	// Watch for events and client connection error conditions
	// (e.g. disconnect).
	for {
		select {
		case e := <-ch:
			if ep, err := EventToProto(e, include, elaborate); err != nil {
				log.Error().Err(err).Msg("failed to marshal event to RPC")
				continue
			} else {
				events := make([]*dst.Event, 0, 1)
				events = append(events, ep)
				resp := &dst.SubscribeResponse{
					Header: &dst.ResponseHeader{
						ReqId: req.Header.ReqId,
					},
					Events: events,
				}
				if err := stream.Send(resp); err != nil {
					break
				}
			}
		case <-stream.Context().Done():
			log.Debug().Msg(fmt.Sprintf("stream closed unexpectedly (%s)", sid))
			return nil
		}
	}

	return nil
}
