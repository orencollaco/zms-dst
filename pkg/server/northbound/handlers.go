// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package northbound

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/policy"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/token"

	dst "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	event "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
	identity "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/observations"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/version"
)

func (s *Server) SetupHealthRoutes() error {
	s.ginHealth.GET("/health/alive", s.GetHealthAlive)
	s.ginHealth.GET("/health/ready", s.GetHealthReady)
	return nil
}

func (s *Server) GetHealthAlive(c *gin.Context) {
	c.Status(http.StatusOK)
}

func (s *Server) GetHealthReady(c *gin.Context) {
	if db, err := s.db.DB(); err == nil {
		if err := db.Ping(); err == nil {
			c.Status(http.StatusOK)
			return
		}
	}
	c.Status(http.StatusServiceUnavailable)
}

func CheckElaborateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Elaborate"); tokenString == "true" {
			c.Set("zms.elaborate", true)
		} else {
			c.Set("zms.elaborate", false)
		}
	}
}

func CheckForceUpdateMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if tokenString := c.GetHeader("X-Api-Force-Update"); tokenString == "true" {
			c.Set("zms.force-update", true)
		} else {
			c.Set("zms.force-update", false)
		}
	}
}

func CheckTokenValue(s *Server, tokenString string) (*client.CachedToken, int, error) {
	if err := token.Validate(tokenString); err != nil {
		return nil, http.StatusBadRequest, fmt.Errorf("invalid token")
	}

	if tok, err := s.rclient.LookupToken(tokenString); tok != nil && err == nil {
		return tok, http.StatusOK, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	var identityClient identity.IdentityClient
	var terr error
	if identityClient, terr = s.rclient.GetIdentityClient(ctx); terr != nil || identityClient == nil {
		return nil, http.StatusBadRequest, fmt.Errorf("identity service unavailable")
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	tt := true
	req := identity.GetTokenRequest{
		Header: &identity.RequestHeader{
			Elaborate: &tt,
		},
		Token: tokenString,
	}
	resp, err := identityClient.GetToken(ctx, &req)
	if err != nil {
		rpcErr, _ := status.FromError(err)
		if rpcErr.Code() == codes.NotFound {
			return nil, http.StatusNotFound, fmt.Errorf("token not found")
		} else if rpcErr.Code() == codes.Unauthenticated {
			return nil, http.StatusUnauthorized, fmt.Errorf(rpcErr.Message())
		} else {
			return nil, http.StatusInternalServerError, fmt.Errorf("identity service error: %v", rpcErr.Message())
		}
	}
	if tok, err := s.rclient.CheckAndCacheToken(resp.Token); err != nil {
		return nil, http.StatusForbidden, fmt.Errorf("invalid token: %s", err.Error())
	} else {
		return tok, http.StatusOK, nil
	}
}

func CheckTokenMiddleware(s *Server) gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		tokenString := c.GetHeader("X-Api-Token")
		if tokenString == "" {
			c.Set("zms.token", nil)
			return
		}
		var tok *client.CachedToken
		var code int
		if tok, code, err = CheckTokenValue(s, tokenString); err != nil {
			c.AbortWithStatusJSON(code, gin.H{"error": err.Error()})
			return
		} else {
			c.Set("zms.token", tok)
		}
	}
}

func (s *Server) CheckPolicyMiddleware(t *client.CachedToken, targetUserId *uuid.UUID, targetElementId *uuid.UUID, policies []policy.Policy) (bool, string) {
	for _, p := range policies {
		log.Debug().Msg(fmt.Sprintf("policy: %s", p.Name))
		for _, trb := range t.RoleBindings {
			if match := p.Check(trb.Role.Value, &t.UserId, trb.ElementId, targetUserId, targetElementId); match == true {
				log.Debug().Msg(fmt.Sprintf("policy match: %s", p.Name))
				return true, p.Name
			} else {
				log.Debug().Msg(fmt.Sprintf("policy mismatch: %s", p.Name))
			}
		}
	}
	return false, ""
}

func (s *Server) RequireContextToken(c *gin.Context) (*client.CachedToken, error) {
	value, exists := c.Get("zms.token")
	if !exists || value == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "missing X-Api-Token"})
		return nil, errors.New("missing X-Api-Token")
	}
	return value.(*client.CachedToken), nil
}

func CorsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func (s *Server) SetupRoutes() error {
	v1 := s.gin.Group("/v1")

	v1.Use(CorsMiddleware())
	v1.Use(CheckElaborateMiddleware())
	v1.Use(CheckForceUpdateMiddleware())
	v1.Use(CheckTokenMiddleware(s))

	v1.GET("/version", s.GetVersion)

	v1.GET("/observations", s.GetObservationList)
	v1.POST("/observations", s.CreateObservation)
	v1.GET("/observations/:observation_id", s.GetObservation)

	v1.GET("/subscriptions", s.GetSubscriptions)
	v1.POST("/subscriptions", s.CreateSubscription)
	v1.DELETE("/subscriptions/:subscription_id", s.DeleteSubscription)
	v1.GET("/subscriptions/:subscription_id/events", s.GetSubscriptionEvents)

	return nil
}

func GetPaginateParams(c *gin.Context) (int, int) {
	page, _ := strconv.Atoi(c.Query("page"))
	if page < 1 {
		page = 1
	}
	itemsPerPage, _ := strconv.Atoi(c.Query("items_per_page"))
	if itemsPerPage < 10 {
		itemsPerPage = 10
	} else if itemsPerPage > 100 {
		itemsPerPage = 100
	}
	return page, itemsPerPage
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func (s *Server) GetVersion(c *gin.Context) {
	if _, err := s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	c.JSON(http.StatusOK, version.GetVersion())
}

type GetObservationListQueryParams struct {
	//Pending gin support...
	//ElementId *string `form:"element_id" binding:"omitempty,uuid"`
	//MonitorId *string `form:"monitor_id" binding:"omitempty,uuid"`
	Observation  *string `form:"observation" binding:"omitempty"`
	Types        *string `form:"types" binding:"omitempty"`
	Format       *string `form:"format" binding:"omitempty"`
	Freq         *int64  `form:"freq" binding:"omitempty"`
	Violation    *bool   `form:"violation" binding:"omitempty"`
	Interference *bool   `form:"interference" binding:"omitempty"`
	Sort         *string `form:"sort" binding:"omitempty,oneof=monitor_id types format starts_at ends_at violation interference created_at updated_at"`
	SortAsc      *bool   `form:"sort_asc" binding:"omitempty"`
}

func (s *Server) GetObservationList(c *gin.Context) {
	var err error

	// Check optional filter query parameters.
	params := GetObservationListQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Unmarshal query uuids.
	var elementId *uuid.UUID
	if p, exists := c.GetQuery("element_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid element_id uuid"})
			return
		} else {
			elementId = &idParsed
		}
	}
	// Unmarshal query uuids.
	var monitorId *uuid.UUID
	if p, exists := c.GetQuery("monitor_id"); exists {
		if idParsed, err := uuid.Parse(p); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "invalid monitor_id uuid"})
			return
		} else {
			monitorId = &idParsed
		}
	}

	page, itemsPerPage := GetPaginateParams(c)

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	var policyName string
	if match, policyName = s.CheckPolicyMiddleware(tok, nil, elementId, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Admins can filter for anything; Viewers (e.g. if they have no element
	// binding) cannot filter; other users who have roles in Elements can
	// filter within those Elements.
	var elementIdList []*uuid.UUID
	if elementId != nil {
		elementIdList = []*uuid.UUID{elementId}
	} else if policyName != policy.MatchAdmin {
		elementIdList = tok.GetElementIds()
	}

	if policyName == policy.MatchViewer && len(elementIdList) == 0 {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "viewer unauthorized (not supported yet)"})
		return
	}

	var observationList []store.Observation
	q := s.db
	if len(elementIdList) > 0 {
		q = q.Where("Observations.element_id in ?", elementIdList)
	}
	if monitorId != nil {
		q = q.Where("Observations.monitor_id = ?", *monitorId)
	}
	if params.Violation != nil {
		if !*params.Violation {
			q = q.Where("not Observations.violation")
		} else {
			q = q.Where("Observations.violation")
		}
	}
	if params.Interference != nil {
		if !*params.Interference {
			q = q.Where("not Observations.interference")
		} else {
			q = q.Where("Observations.interference")
		}
	}
	if params.Freq != nil {
		q = q.Where("Observations.min_freq <= ?", *params.Freq).
			Where("(Observations.max_freq is NULL or Observations.max_freq > ?)", *params.Freq)
	}
	if params.Observation != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Observations.description"), fmt.Sprintf("%%%s%%", *params.Observation))
	}
	if params.Types != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Observations.types"), fmt.Sprintf("%%%s%%", *params.Types))
	}
	if params.Format != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Observations.format"), fmt.Sprintf("%%%s%%", *params.Format))
	}
	sortDir := "desc"
	if params.SortAsc != nil && *params.SortAsc {
		sortDir = "asc"
	}
	if params.Sort != nil {
		q = q.Order(*params.Sort + " " + sortDir)
	} else {
		q = q.Order("updated_at " + sortDir)
	}

	q = q.Omit("Data")

	var total int64
	cres := q.Model(&store.Observation{}).Count(&total)
	if cres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	qres := q.Scopes(store.Paginate(page, itemsPerPage)).Find(&observationList)
	if qres.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": cres.Error.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"observations": observationList, "page": page, "total": total, "pages": int(math.Ceil(float64(total) / float64(itemsPerPage)))})
}

func (s *Server) HandleObservation(ctx context.Context, observation *store.Observation, monitor *zmc.Monitor) (err error) {
	if !observation.Violation {
		return nil
	}
	if monitor.MonitoredRadioPortId == nil {
		return nil
	}

	// Find a grant associated with the observation time and monitored radio
	// port; if there is one, update the violation verification timestamp.

	var zmcClient zmc.ZmcClient
	if zmcClient, err = s.rclient.GetZmcClient(ctx); err != nil {
		return status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}

	ff := false
	reqId := uuid.New().String()
	req := zmc.GetGrantsRequest{
		Header: &zmc.RequestHeader{
			ReqId:     &reqId,
			Elaborate: &ff,
		},
		RadioPortId: monitor.MonitoredRadioPortId,
		Deleted:     &ff,
	}
	var grant *zmc.Grant
	var resp *zmc.GetGrantsResponse
	if resp, err = zmcClient.GetGrants(ctx, &req); err != nil {
		return status.Errorf(codes.Internal, "Failed to obtain grants from zmc service")
	} else if resp.Grants == nil || len(resp.Grants) == 0 {
		return status.Errorf(codes.Internal, "No grants available from zmc service")
	} else {
		grant = resp.Grants[0]
	}

	if idParsed, err := uuid.Parse(grant.Id); err != nil {
		log.Error().Err(err).Msg(fmt.Sprintf("failed to parse grant ID (%+v); cannot verify observation", grant))
	} else {
		t := time.Now().UTC()
		observation.ViolationVerifiedAt = &t
		observation.ViolatedGrantId = &idParsed

		log.Info().Msg(fmt.Sprintf("observation %s: violated grant %s", observation.Id, grant.Id))
	}

	return nil
}

func WarnOnlySession(db *gorm.DB) (tx *gorm.DB) {
	return db.Session(&gorm.Session{
		Logger: db.Logger.LogMode(logger.Warn),
	})
}

func (s *Server) CreateObservation(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm store.Observation
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var monitor *zmc.Monitor
	if monitor, err = s.rclient.GetMonitor(c, &dm.MonitorId, true); err != nil {
		log.Error().Msg("failed to get monitor from zmc")
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("failed to get monitor %v from zmc service", dm.MonitorId)})
		return
	}
	var creatorId uuid.UUID
	if creatorId, err = uuid.Parse(monitor.CreatorId); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "bogus internal monitor.CreatorId"})
	}
	var radioPortId uuid.UUID
	if radioPortId, err = uuid.Parse(monitor.RadioPortId); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "bogus internal monitor.RadioPortId"})
		return
	}

	var radioPort *zmc.RadioPort
	if radioPort, err = s.rclient.GetRadioPort(c, &radioPortId, true); err != nil {
		log.Error().Msg("failed to get radio port from zmc")
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("failed to get radio port %v from zmc service", monitor.RadioPortId)})
		return
	}
	var radioId uuid.UUID
	if radioId, err = uuid.Parse(radioPort.RadioId); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "bogus internal radioPort.RadioId"})
		return
	}

	var radio *zmc.Radio
	if radio, err = s.rclient.GetRadio(c, &radioId, true); err != nil {
		log.Error().Msg("failed to get radio from zmc")
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": fmt.Sprintf("failed to get radio %v from zmc service", radioId)})
		return
	}

	var elementId uuid.UUID
	if elementId, err = uuid.Parse(radio.ElementId); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "bogus internal radio.ElementId"})
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRolePolicy(policy.RoleOperator, policy.GreaterOrEqual)
	match, _ := s.CheckPolicyMiddleware(tok, &creatorId, &elementId, policies)
	if !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	// Create an observation, overriding or forcing bits that should not be set
	// by caller.
	// XXX: need to verify GrantConstraint eventually...
	observation := &store.Observation{
		Id: uuid.New(), MonitorId: dm.MonitorId, Types: dm.Types, Format: dm.Format,
		Description: dm.Description, MinFreq: dm.MinFreq, MaxFreq: dm.MaxFreq, FreqStep: dm.FreqStep,
		StartsAt: dm.StartsAt, EndsAt: dm.EndsAt, Data: dm.Data,
		Violation: dm.Violation, Interference: dm.Interference,
		ElementId: elementId, CreatorId: tok.UserId,
	}

	// Process the observation
	var annotations []*store.Annotation
	if p := observations.ProcessorFactory(observation); p != nil {
		if err = p.Check(observation); err != nil {
			msg := fmt.Sprintf("observation processor.Check failed: %s (monitor %s)", err.Error(), dm.MonitorId.String())
			log.Warn().Msg(msg)
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": msg})
			return
		}
		if _, err = p.Update(observation); err != nil {
			msg := fmt.Sprintf("observation processor.Check failed: %s (monitor %s)", err.Error(), dm.MonitorId.String())
			log.Warn().Msg(msg)
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": msg})
			return
		}
		if annotations, err = p.Annotate(observation); err != nil {
			msg := fmt.Sprintf("observation processor.Annotate failed: %s (monitor %s)", err.Error(), dm.MonitorId.String())
			log.Warn().Msg(msg)
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": msg})
			return
		}
		if annotations != nil {
			//observation.Annotations = make([]store.Annotation, 0, len(annotations))
			for _, a := range annotations {
				a.CreatorId = observation.CreatorId
				observation.Annotations = append(observation.Annotations, *a)
			}
		}
	} else {
		log.Debug().Msg(fmt.Sprintf("not processing observation"))
	}

	// Write the data to persistent storage instead of keeping it in the
	// database.
	if len(observation.Data) > 0 {
		if err = s.WriteObservationData(observation); err != nil {
			msg := fmt.Sprintf("failed to save observation data (monitor %s)", observation.MonitorId.String())
			log.Error().Err(err).Msg(msg)
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		observation.Data = make([]byte, 0)
	}

	collection := store.Collection{
		Id: uuid.New(), MonitorId: observation.MonitorId, Types: observation.Types, Format: observation.Format,
		Description: observation.Description, MinFreq: observation.MinFreq, MaxFreq: observation.MaxFreq,
		StartsAt: observation.StartsAt, EndsAt: observation.EndsAt, Violation: observation.Violation,
		Interference: observation.Interference, ElementId: observation.ElementId, CreatorId: observation.CreatorId,
		UpdaterId: observation.UpdaterId, CreatedAt: observation.CreatedAt, UpdatedAt: observation.UpdatedAt,
	}
	if res := s.db.Create(&collection); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	}
	observation.CollectionId = &collection.Id
	if res := WarnOnlySession(s.db).Create(observation); res.Error != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": res.Error.Error()})
		return
	}

	// If this is a violation from an inline monitor, see if we can
	// trivially associate the grant.
	s.HandleObservation(c, observation, monitor)

	if !c.GetBool("zms.elaborate") {
		observation.Annotations = nil
	}

	c.JSON(http.StatusOK, observation)

	observation.Annotations = nil

	if observation.ViolationVerifiedAt != nil {
		// Generate an event.
		oid := observation.Id.String()
		eh := subscription.EventHeader{
			Type:       int32(event.EventType_ET_VIOLATION),
			Code:       int32(dst.EventCode_EC_OBSERVATION),
			SourceType: int32(event.EventSourceType_EST_DST),
			SourceId:   s.config.ServiceId,
			Id:         uuid.New().String(),
			ObjectId:   &oid,
			Time:       &observation.StartsAt,
		}
		e := subscription.Event{
			Header: eh,
			Object: observation,
		}
		go s.sm.Notify(&e)
	} else {
		// Send an observation event.
		oid := observation.Id.String()
		eh := subscription.EventHeader{
			Type:       int32(event.EventType_ET_CREATED),
			Code:       int32(dst.EventCode_EC_OBSERVATION),
			SourceType: int32(event.EventSourceType_EST_DST),
			SourceId:   s.config.ServiceId,
			Id:         uuid.New().String(),
			ObjectId:   &oid,
			Time:       &dm.StartsAt,
		}
		e := subscription.Event{
			Header: eh,
			Object: observation,
		}
		go s.sm.Notify(&e)
	}

	return
}

func (s *Server) WriteObservationData(o *store.Observation) (err error) {
	dateString := time.Now().Format(time.DateOnly)
	destDir := fmt.Sprintf("%s/observations/%s/%s/%s", s.config.DataDir, o.MonitorId.String(), dateString, o.Id.String())
	if err = os.MkdirAll(destDir, 0750); err != nil {
		log.Error().Err(err).Msg(fmt.Sprintf("failed to create observation data dir %v", destDir))
		return err
	}
	destFile := destDir + "/data"
	if err = os.WriteFile(destFile, o.Data, 0640); err != nil {
		log.Error().Err(err).Msg(fmt.Sprintf("failed to write observation data file %v", destFile))
		return err
	}
	o.Path = destFile
	return nil
}

type GetObservationQueryParams struct {
	DataInline    *bool   `form:"data_inline" binding:"omitempty"`
}

func (s *Server) GetObservation(c *gin.Context) {
	var err error

	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("observation_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	} else {
		id = idParsed
	}

	// Check optional filter query parameters.
	params := GetObservationQueryParams{}
	if err = c.ShouldBindQuery(&params); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	policies := policy.MakeMatchAdminOrElementRoleOrViewerPolicy(policy.RoleViewer, policy.GreaterOrEqual)
	var match bool
	if match, _ = s.CheckPolicyMiddleware(tok, nil, nil, policies); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	var observation store.Observation
	q := s.db
	if c.GetBool("zms.elaborate") {
		q = q.Preload("Annotations")
	}
	res := q.First(&observation, id)
	if res.RowsAffected != 1 {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	// We never send data unless explicitly requested.
	if params.DataInline != nil && *params.DataInline {
		if observation.Path != "" {
			if observation.Data, err = os.ReadFile(observation.Path); err != nil {
				log.Error().Err(err).Msg(fmt.Sprintf("failed to read observation data file %v (%v)", observation.Path, observation.Id))
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "failed to read observation data file"})
				return
			}
		}
	} else {
		observation.Data = make([]byte, 0)
	}

	c.JSON(http.StatusOK, observation)
}

type CreateSubscriptionModel struct {
	Id      string                     `json:"id" binding:"required,uuid"`
	Filters []subscription.EventFilter `json:"filters" binding:"omitempty"`
}

func (s *Server) CreateSubscription(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Initial input validation.
	var dm CreateSubscriptionModel
	if err := c.ShouldBindJSON(&dm); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// NB: for now, non-admins will be restricted to self-user-associated
	// events.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		// Each filter must have UserIds set to the calling user, and must
		// have ElementIds set to a subset of the ElementIds in the token.
		if dm.Filters == nil || len(dm.Filters) < 1 {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must provide at least one filter, with UserIds set to exactly their user ID, and ElementIds to a subset of the calling token's element IDs."})
			return
		}
		for _, filter := range dm.Filters {
			if filter.UserIds == nil || len(filter.UserIds) != 1 || filter.UserIds[0] != tok.UserId.String() {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must set each filter's UserIds field to a one-item list containing exactly their user ID."})
				return
			}
			if filter.ElementIds == nil || len(filter.ElementIds) < 1 {
				c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must set each filter's ElementIds field to a subset of the ElementIds in the calling token."})
				return
			}
			for _, elementId := range filter.ElementIds {
				found := false
				for _, trb := range tok.RoleBindings {
					if trb.ElementId == nil {
						continue
					} else if trb.ElementId.String() == elementId {
						found = true
						break
					}
				}
				if !found {
					c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "non-admin user must set each filter's ElementIds field to a subset of the ElementIds in the calling token."})
					return
				}
			}
		}
	}

	if err := s.sm.Subscribe(dm.Id, dm.Filters, nil, c.ClientIP(), subscription.Rest, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusAccepted)

	return
}

func (s *Server) GetSubscriptions(c *gin.Context) {
	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"subscriptions": s.sm.GetSubscriptions()})
	return
}

var wsUpgrader = websocket.Upgrader{
	ReadBufferSize:  0,
	WriteBufferSize: 16384,
	Subprotocols:    nil,
}

func (s *Server) GetSubscriptionEvents(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	var subprotos []string
	if xApiTok := c.GetHeader("X-Api-Token"); xApiTok != "" {
		if tok, err = s.RequireContextToken(c); err != nil {
			return
		}
	} else if wsProto := c.GetHeader("Sec-WebSocket-Protocol"); wsProto != "" {
		if wsTok, code, altErr := CheckTokenValue(s, wsProto); altErr != nil {
			log.Warn().Msg(fmt.Sprintf("bad token (%s) in Sec-WebSocket-Protocol: %+v", wsProto, altErr.Error()))
			c.AbortWithStatusJSON(code, gin.H{"error": altErr.Error()})
			return
		} else {
			tok = wsTok
			subprotos = append(subprotos, wsProto)
		}
	}
	if tok == nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": "no token provided"})
		return
	} else {
		log.Debug().Msg(fmt.Sprintf("valid token from websocket"))
	}

	ech := make(chan *subscription.Event)
	if err := s.sm.UpdateChannel(idStr, ech, &tok.Token); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// See if client wants to reuse subscription upon websocket close.
	deleteOnClose := true
	if tokenString := c.GetHeader("X-Api-Delete-On-Close"); tokenString == "false" {
		c.Set("zms.deleteOnClose", false)
		deleteOnClose = false
	} else {
		c.Set("zms.deleteOnClose", true)
	}

	defer func() {
		if deleteOnClose {
			s.sm.Unsubscribe(idStr)
		} else {
			log.Debug().Msg(fmt.Sprintf("retaining subscription %s", idStr))
			s.sm.UpdateChannel(idStr, nil, &tok.Token)
		}
	}()

	wsUpgrader := websocket.Upgrader{
		ReadBufferSize: 0,
		WriteBufferSize: 16384,
		Subprotocols: subprotos,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}

	var conn *websocket.Conn
	if conn, err = wsUpgrader.Upgrade(c.Writer, c.Request, nil); err != nil {
		log.Debug().Msg(fmt.Sprintf("error upgrading websocket conn: %+v", err.Error()))
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Handle client disconnect.
	cch := make(chan interface{})
	go func() {
		for {
			_, _, err := conn.ReadMessage()
			if err != nil {
				cch <- nil
				conn.Close()
				return
			}
		}
	}()

	// Read events from SubscriptionManager, and client read errors or
	// client disconnection.
	for {
		select {
		case e := <-ech:
			if body, jErr := json.Marshal(e); jErr != nil {
				log.Error().Err(jErr).Msg(fmt.Sprintf("error marshaling event (%+v): %s", e, err.Error()))
				continue
			} else {
				log.Debug().Msg(fmt.Sprintf("marshaled event: %+v", body))
				if err = conn.WriteMessage(websocket.TextMessage, body); err != nil {
					return
				}
			}
		case <-cch:
			// NB: the deferred handler covers this case.
			return
		}
	}

	return
}

func (s *Server) DeleteSubscription(c *gin.Context) {
	// Check required path parameters.
	var id uuid.UUID
	if idParsed, err := uuid.Parse(c.Param("subscription_id")); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
	} else {
		id = idParsed
	}
	idStr := id.String()

	// Ensure caller provided a valid token.
	var tok *client.CachedToken
	var err error
	if tok, err = s.RequireContextToken(c); err != nil {
		return
	}

	var sub *subscription.Subscription[*subscription.Event]
	if sub = s.sm.GetSubscription(idStr); sub == nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	// Check policy.
	if match, _ := s.CheckPolicyMiddleware(tok, nil, nil, policy.AdminOnlyPolicy); !match {
		if sub.Token == nil || *sub.Token != tok.Token {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"error": "unauthorized"})
			return
		}
	}

	if err := s.sm.Unsubscribe(idStr); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": "not found"})
		return
	}

	c.Status(http.StatusAccepted)

	return
}
